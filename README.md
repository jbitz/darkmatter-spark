Darkmatter-Spark
========
----------
This repository contains all the code used for the ongoing Dark Matter project, from the Stanford University HCI group, which seeks to measure discrepancies in the distribution of political opinions on- and off-line. Most of this code is intended to be run on a Spark cluster (e.g. Amazon EMR) for parallelization - sweeping over archives of the entire internet is not a single-machine task. The data all comes from the [CommonCrawl Archives](https://commoncrawl.org/). For questions, please message jbitz (at) stanford.edu.

