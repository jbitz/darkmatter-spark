/*
 * CCClassifier.java
 * 
 * Spark app that goes through the entire commoncrawl archive, and saves those pages classified
 * as relevant by the specified classifier
 * 
 * Usage: spark-submit ... [topic] [model] [protocol] ([segments])
 * topic: Name of the darkmatter-testing sub-bucket in S3
 * model: the number of the logistic regression model from s3 to use
 * 		(stored under s3://darkmatter-testing/[topic]/models/)
 * protocol: 's3n' if running locally, 's3' if running on EMR
 * segments (optional): the number of CommonCrawl segments to traverse
 * 						if none are specified, all of CC is used
 * 
 * Recommendation - after running this script, download the results with
 *  /var/darkmatter/downloader/downloadResults.py (on the EC2 server)
 *  Give it the topic name, and it will pull everything together in a nice 
 *  single text file of URLs
 */

package com.jaredbitz.darkmatter;

import java.util.ArrayList;

import java.util.List;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.*;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.ml.*;
import org.apache.spark.sql.*;
import org.apache.spark.sql.types.*;

import scala.Tuple2;

import com.jaredbitz.darkmatter.functions.*;

public class CCClassifier {
	
	public static void main(String[] args) {
		
		//Initialize constants from command-line args
		final String DARKMATTER_BUCKET = "darkmatter-training"; //Bucket it S3 
		final String DARKMATTER_TOPIC = args[0]; //Sub-folder containing training examples 
		final String S3_PROTOCOL = args[2] + "://"; //"s3n://" if running locally, "s3://" if running on EMR  
		final String DARKMATTER_PREFIX = S3_PROTOCOL + DARKMATTER_BUCKET + '/' + DARKMATTER_TOPIC + '/';
		final String CLASSIFIER_PATH = DARKMATTER_PREFIX + "models/model" + Integer.parseInt(args[1]) + ".ml";
		final String WET_FILE_LIST = S3_PROTOCOL + 
				"commoncrawl/crawl-data/CC-MAIN-2016-22/wet.paths.gz"; //Master path file (from commoncrawl website)

		//Load SparkContext
		SparkConf conf = new SparkConf().setAppName("Dark Matter CommonCrawl Classification");
    	try {
    		conf.registerKryoClasses(new Class<?>[]{
    			Class.forName("org.apache.hadoop.io.LongWritable"),
    			Class.forName("org.apache.hadoop.io.Text")
    		});
    	} catch (ClassNotFoundException e) { //Should never happen, just java being safe
    		e.printStackTrace();
    	}
    	JavaSparkContext sc = new JavaSparkContext(conf);
    	SQLContext sqlContext = new SQLContext(sc); //For DataFrames later
    	
    	//So that we can properly split the WARC files by page
    	//"WARC/1.0" is the first line of the header of each individual page in the file
    	Configuration s3Conf = new Configuration();
        s3Conf.set("textinputformat.record.delimiter", "WARC/1.0");

    	/* Load list of all CommonCrawl URLs - there's 24,492 in the most recent one
    	 * The loading of the files themselves is not parallelized - that kind of 
    	 * goes against the driver-slave idea, because the slave would become mini-
    	 * drivers in and of themselves. Instead, we load the files linearly, and 
    	 * processing the individual files happens in parallel
    	 */
       
    	List<String> ccURL;
    	if(args.length > 3) //If only running on a segment of CC, just take those files
    		ccURL = sc.textFile(WET_FILE_LIST).take(Integer.parseInt(args[3]));
    	else
    		ccURL = sc.textFile(WET_FILE_LIST).collect();
    	
    	
    	//Prepare the ML pipeline and it's infrastructure
    	PipelineModel classifier = PipelineModel.load(CLASSIFIER_PATH);
    	DataFrame allData = createDataFrame(null, sc, sqlContext, true);
    	
    	for(String s : ccURL) {
    		
    		//Obtain WET file from the URL, and filter down to pages of English text
            JavaRDD<Tuple2<String, String>> pages = sc.newAPIHadoopFile(S3_PROTOCOL 
            		+ "commoncrawl/" + s, org.apache.hadoop.mapreduce.lib.input.TextInputFormat.class, 
            		LongWritable.class, Text.class, s3Conf)
            		.filter(new WETFilter())
            		.map(new WETTransformerWithURL());
            
            //For local testing, so my computer doesn't explode
            //pages = sc.parallelize(pages.take(10));
         
            //Process ALL THE THINGS
            DataFrame dataFrame = createDataFrame(pages, sc, sqlContext, false);
            DataFrame processed = classifier.transform(dataFrame);
           
            //SQL Query to keep the URL's of relevant pages
            processed = processed.select("url", "prediction") //We only care about the url and its relevance
            		.where(processed.col("prediction").gt(.9)); //Choose only URLs marked relevant
                     
            allData = allData.unionAll(processed);
    	}
    	allData.select("url").write().text(DARKMATTER_PREFIX + "cc_pages/pages_classifier.txt");
    	//System.out.println(x);
	}
	
	
	private static DataFrame createDataFrame(JavaRDD<Tuple2<String, String>> data, JavaSparkContext sc, 
			SQLContext sqlContext, boolean empty) {
		List<StructField> fields = new ArrayList<StructField>();
		fields.add(DataTypes.createStructField("url", DataTypes.StringType, false));
        fields.add(DataTypes.createStructField("text",  DataTypes.StringType, false));
        StructType schema = DataTypes.createStructType(fields);
        
        if(empty) return sqlContext.createDataFrame(new ArrayList<Row>(), schema);
        
        @SuppressWarnings("unchecked")
		JavaRDD<Row> rows = data.map( new Function<Tuple2<String, String>, Row>() {
			public Row call(Tuple2<String, String> record) throws Exception {
					return RowFactory.create(record._1, record._2);
        	}
        });
        
       return sqlContext.createDataFrame(rows, schema);
	}
}

