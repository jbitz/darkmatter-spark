/* CCClassifier.java
 * 
 * Spark app that goes through the entire commoncrawl archive, and saves those pages classified
 * as relevant by the specified classifier
 * 
 * Usage: spark-submit ... [topic] [keyword-file] [protocol] ([segments])
 * topic: Name of the darkmatter-testing sub-bucket in S3
 * keyword-file: name of the keyword whitelist file
 * protocol: 's3n' if running locally, 's3' if running on EMR
 * segments (optional): the number of CommonCrawl segments to traverse
 * 						if none are specified, all of CC is used
 * 
 * Recommendation - after running this script, download the results with
 *  /var/darkmatter/downloader/downloadResults.py (on the EC2 server)
 *  Give it the topic name, and it will pull everything together in a nice 
 *  single text file of URLs
 */

/*package com.jaredbitz.darkmatter;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.ArrayList;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.spark.SparkConf;
import org.apache.spark.Accumulator;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.VoidFunction;
import org.apache.spark.sql.Row;

import com.jaredbitz.darkmatter.functions.*;

import scala.Tuple2;

public class CCIDFScore {

	public static void main(String[] args) {

		//Initialize constants from command-line args
		final String DARKMATTER_BUCKET = "darkmatter-training"; //Bucket it S3 
		final String DARKMATTER_TOPIC = args[0]; //Sub-folder containing training examples 
		final String S3_PROTOCOL = args[2] + "://"; //"s3n://" if running locally, "s3://" if running on EMR  
		final String DARKMATTER_PREFIX = S3_PROTOCOL + DARKMATTER_BUCKET + '/' + DARKMATTER_TOPIC + '/';
		final String WHITELIST_PATH = DARKMATTER_PREFIX + "models/" + args[1];
		final String WET_FILE_LIST = S3_PROTOCOL + 
				"commoncrawl/crawl-data/CC-MAIN-2016-22/wet.paths.gz"; //Master path file (from commoncrawl website)

		//Load SparkContext
		SparkConf conf = new SparkConf().setAppName("Dark Matter " + DARKMATTER_TOPIC + " Crawl");
		try {
			conf.registerKryoClasses(new Class<?>[]{
				Class.forName("org.apache.hadoop.io.LongWritable"),
				Class.forName("org.apache.hadoop.io.Text")
			});
		} catch (ClassNotFoundException e) { //Should never happen, just java being safe
			e.printStackTrace();
		}
		JavaSparkContext sc = new JavaSparkContext(conf);

		//So that we can properly split the WARC files by page
		//"WARC/1.0" is the first line of the header of each individual page in the file
		Configuration s3Conf = new Configuration();
		s3Conf.set("textinputformat.record.delimiter", "WARC/1.0");
		*/

		/* Load list of all CommonCrawl URLs - there's 24,492 in the most recent one
		 * The loading of the files themselves is not parallelized - that kind of 
		 * goes against the driver-slave idea, because the slave would become mini-
		 * drivers in and of themselves. Instead, we load the files linearly, and 
		 * processing the individual files happens in parallel
		 */
	/*
		List<String> ccURL;
		if(args.length > 3) //If only running on a segment of CC, just take those files
			ccURL = sc.textFile(WET_FILE_LIST).take(Integer.parseInt(args[3]));
		else
			ccURL = sc.textFile(WET_FILE_LIST).collect();


		//Prepare the ML pipeline and it's infrastructure
		final List<String> whitelist = sc.textFile(WHITELIST_PATH).collect();
		final HashMap<String, Accumulator<Integer>> accumulators = new HashMap<String, Accumulator<Integer>>();

		for(String s : whitelist) {
			accumulators.put(s, sc.accumulator(0));
		}


		final IDFCounterTransformer<String> counterFunction = new IDFCounterTransformer<String>(whitelist);


		//System.out.println(whitelist.size());

		int totalDocuments = 0;
		int counter = 0;
		for(String s : ccURL) {

			//Obtain WET file from the URL, and filter down to pages of English text
			JavaRDD<Tuple2<String, String>> pages = 
					sc.newAPIHadoopFile(S3_PROTOCOL 
							+ "commoncrawl/" + s, org.apache.hadoop.mapreduce.lib.input.TextInputFormat.class, 
							LongWritable.class, Text.class, s3Conf)
					.filter(new WETFilter())
					.map(new WETTransformerWithURL());

			//Count unique occurrences in each document
			pages.foreach(new VoidFunction<Tuple2<String, String>>() {
				public void call(Tuple2<String, String> doc){
					HashSet<String> h = new HashSet<String>();
					for(String w : doc._2.split(" ")){
						if(whitelist.contains(w) && !h.contains(w)) {
							h.add(w);
							accumulators.get(w).add(1);
						}
					}
				}
			});


			totalDocuments += pages.count();
		}

		ArrayList<Tuple2<String, Integer>> counts = new ArrayList<Tuple2<String, Integer>>();
		final int totalDocumentCount = totalDocuments;
		for(String s : accumulators.keySet())
			counts.add(new Tuple2<String, Integer>(s, accumulators.get(s).value()));
		JavaRDD<Tuple2<String, Double>> countsRDD = sc.parallelize(counts)
				.map(new Function<Tuple2<String, Integer>, Tuple2<String, Double>>() {
					public Tuple2<String, Double> call(Tuple2<String, Integer> pairing){
						return new Tuple2<String, Double>(pairing._1, Math.log(((double)totalDocumentCount) / ((double)pairing._2)));
					}
				});
		//countsRDD.saveAsTextFile("test.txt");
		countsRDD.saveAsTextFile(DARKMATTER_PREFIX + "models/whitelist_IDF.txt");

		sc.close();
	}
}
*/
