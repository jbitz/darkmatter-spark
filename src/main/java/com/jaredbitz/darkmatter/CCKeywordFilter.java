/*
 * CCKeywordFilter.java
 * 
 * Spark app that goes through the entire commoncrawl archive, and saves those pages classified
 * as relevant by a weighted TF threshold (see WhitelistFilter.java)
 * 
 * Usage: spark-submit ... [topic] [keyword-file] [protocol] ([segments])
 * topic: Name of the darkmatter-testing sub-bucket in S3
 * keyword-file: name of the keyword whitelist file - a csv of form [term],[weight]
 * protocol: 's3n' if running locally, 's3' if running on EMR
 * segments (optional): the number of CommonCrawl segments to traverse
 * 						if not specified, all of CC is used
 * 
 * Recommendation - after running this script, download the results with
 *  /var/darkmatter/downloader/downloadResults.py (on the EC2 server)
 *  Give it the topic name, and it will pull everything together in a nice 
 *  single text file of URLs
 */

package com.jaredbitz.darkmatter;

import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.spark.SparkConf;
import org.apache.spark.Accumulator;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;

import com.jaredbitz.darkmatter.functions.*;

import scala.Tuple2;

public class CCKeywordFilter {

	//Change to set the number of matches required for classification as relevant
	private static final double WHITELIST_THRESHOLD = 1.9;

	public static void main(String[] args) {

		//Initialize constants from command-line args
		final String DARKMATTER_BUCKET = "darkmatter-training"; //Bucket it S3 
		final String DARKMATTER_TOPIC = args[0]; //Sub-folder containing training examples 
		final String S3_PROTOCOL = args[2] + "://"; //"s3n://" if running locally, "s3://" if running on EMR  
		final String DARKMATTER_PREFIX = S3_PROTOCOL + DARKMATTER_BUCKET + '/' + DARKMATTER_TOPIC + '/';
		final String WHITELIST_PATH = DARKMATTER_PREFIX + "models/" + args[1];
		final String WET_FILE_LIST = S3_PROTOCOL + 
				"commoncrawl/crawl-data/CC-MAIN-2016-22/wet.paths.gz"; //Master path file (from commoncrawl website)

		//Load SparkContext
		SparkConf conf = new SparkConf().setAppName("Dark Matter " + DARKMATTER_TOPIC + " Crawl");
		try {
			conf.registerKryoClasses(new Class<?>[]{
				Class.forName("org.apache.hadoop.io.LongWritable"),
				Class.forName("org.apache.hadoop.io.Text")
			});
		} catch (ClassNotFoundException e) { //Should never happen, just java being safe
			e.printStackTrace();
		}
		JavaSparkContext sc = new JavaSparkContext(conf);

		//So that we can properly split the WARC files by page
		//"WARC/1.0" is the first line of the header of each individual page in the file
		Configuration s3Conf = new Configuration();
		s3Conf.set("textinputformat.record.delimiter", "WARC/1.0");

		/* Load list of all CommonCrawl URLs - there's 24,492 in the most recent one
		 * The loading of the files themselves is not parallelized - that kind of 
		 * goes against the driver-slave idea, because the slaves would become mini-
		 * drivers in and of themselves. Instead, we load the files linearly, and 
		 * processing the individual files happens in parallel
		 */

		List<String> ccURL;
		if(args.length > 3) //If only running on a segment of CC, just take those files
			ccURL = sc.textFile(WET_FILE_LIST).take(Integer.parseInt(args[3]));
		else
			ccURL = sc.textFile(WET_FILE_LIST).collect();


		//Prepare the whitelist terms and their respective weights
		final List<String> whitelist = sc.textFile(WHITELIST_PATH).collect();
		final HashMap<String, Double> termWeights = new HashMap<String, Double>();
		//The accumulators are used to track which terms cause the most matches
		HashMap<String, Accumulator<Integer>> accumulators = new HashMap<String, Accumulator<Integer>>();

		for(String s : whitelist) {
			String term = s.substring(0, s.lastIndexOf(","));
			double weight = Double.parseDouble(s.substring(s.lastIndexOf(",") + 1));
			termWeights.put(term, weight);
			accumulators.put(term, sc.accumulator(0));
		}
		
		//See WhitelistFilter.java for the details of the filter
		final WhitelistFilter<String> filter = new WhitelistFilter<String>(WHITELIST_THRESHOLD, termWeights, accumulators);

		//Each URL points to a gzip-compressed text file in the commoncrawl s3 bcuket
		//With ~40,000 pages per archive
		int counter = 0;
		for(String s : ccURL) {

			//Obtain WET file from the URL, and filter down to pages of English text
			JavaRDD<String> pages = JavaPairRDD.fromJavaRDD( 
					sc.newAPIHadoopFile(S3_PROTOCOL 
							+ "commoncrawl/" + s, org.apache.hadoop.mapreduce.lib.input.TextInputFormat.class, 
							LongWritable.class, Text.class, s3Conf)
					.filter(new WETFilter())
					.map(new WETTransformerWithURL())
					.filter(filter)).keys();
			//*/

			/* For recall testing - pulls from the verified test set in the darkmatter s3 bucket
			JavaRDD<String> pages =
					JavaPairRDD.fromJavaRDD(
					sc.wholeTextFiles(DARKMATTER_PREFIX + "test-set/")
					.map( new Function<Tuple2<String, String>, Tuple2<String, String>>() {
						public Tuple2<String, String> call(Tuple2<String, String> record) throws Exception {
							return new Tuple2<String, String>("lololol", record._2);
						}
					}).filter(filter)).keys(); */


			//sc.parallelize(pages.take(100)).saveAsTextFile("pages.txt");
			pages.saveAsTextFile(DARKMATTER_PREFIX + "cc_pages_tf/pages_whitelist" + (counter++) + ".txt");
		}

		//Save list of counts for later analysis
		ArrayList<Tuple2<String, Integer>> counts = new ArrayList<Tuple2<String, Integer>>();
		for(String s : accumulators.keySet())
			counts.add(new Tuple2<String, Integer>(s, accumulators.get(s).value()));
		sc.parallelize(counts).saveAsTextFile(DARKMATTER_PREFIX + "models/tests/whitelist_counts_" + args[1]);
		//sc.parallelize(counts).saveAsTextFile("counts.txt");
		sc.close();
	}
}
