/*
 * Spark app that goes through the S3 bucket determined by
 * the DARKMATTER_TOPIC constant and tests a logistic classifier
 * on the test set given in the bucket. 
 * 
 * USAGE: Takes three command line arguments:
 * spark-submit ... [topic] ["model1 model2 model3 ..."] [fs]
 * 
 * topic: The subfolder within the s3 bucket containing the positive examples
 * model1, model2, ...: The names of the models to test, e.g. "model500.ml model1000.ml model1500.ml"
 * fs: "s3n" if running locally, "s3" if running on EMR
 */

package com.jaredbitz.darkmatter;

import java.util.ArrayList;
import java.util.List;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.spark.Accumulator;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.*;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.VoidFunction;
import org.apache.spark.ml.*;
import org.apache.spark.sql.*;
import org.apache.spark.sql.types.*;

import scala.Tuple2;

import com.jaredbitz.darkmatter.functions.*;

public class StageOneClassifierTester {

	private static final String DARKMATTER_BUCKET = "darkmatter-training"; //Bucket in S3 
	static String DARKMATTER_TOPIC;
	static String S3_PROTOCOL;
	static String DARKMATTER_PREFIX;
	static String WET_FILE_LIST;

	public static void main(String[] args) {
		System.out.println("start");
		//-------------------------LOAD ALL THE THINGS----------------------------
		DARKMATTER_TOPIC = args[0]; //Sub-folder containing training examples 
		S3_PROTOCOL = args[2] + "://"; //"s3n://" if running locally, "s3://" if running on EMR  
		DARKMATTER_PREFIX = S3_PROTOCOL + DARKMATTER_BUCKET + '/' + DARKMATTER_TOPIC + '/';
		WET_FILE_LIST = S3_PROTOCOL + "commoncrawl/crawl-data/CC-MAIN-2016-22/wet.paths.gz"; //Master path file (from commoncrawl website)

		SparkConf conf = new SparkConf().setAppName("Dark Matter Classifier Validator");
		try {
			conf.registerKryoClasses(new Class<?>[]{
				Class.forName("org.apache.hadoop.io.LongWritable"),
				Class.forName("org.apache.hadoop.io.Text")
			});
		} catch (ClassNotFoundException e) { //Should never happen, just java being safe
			e.printStackTrace();
		}

		JavaSparkContext sc = new JavaSparkContext(conf);
		SQLContext sqlContext = new SQLContext(sc); //For DataFrames later
		System.out.println("Initialized sc");

		//---------------------------LOAD TEST SET--------------------------
		JavaPairRDD<Double, String> positiveData = JavaPairRDD.fromJavaRDD(
				sc.wholeTextFiles(DARKMATTER_PREFIX + "test-set").map(new PositiveTransformer()));

		//Choose a random segment
		String ccURL = sc.textFile(WET_FILE_LIST).take(17).get(16);

		//Split WET file by page, rather than by line
		Configuration s3Conf = new Configuration();
		s3Conf.set("textinputformat.record.delimiter", "WARC/1.0");

		//Obtain WET file for negative (essentially random) examples and put it in the right format
		JavaPairRDD<LongWritable, Text> negativeExamples = sc.newAPIHadoopFile(S3_PROTOCOL 
				+ "commoncrawl/" + ccURL, org.apache.hadoop.mapreduce.lib.input.TextInputFormat.class, 
				LongWritable.class, Text.class, s3Conf);

		negativeExamples = negativeExamples.filter(new WETFilter());
		JavaPairRDD<Double, String> negativeData = sc.parallelizePairs(
				negativeExamples.map(new WETTransformer()).take((int)positiveData.count()));



		//--------------------Create Dataframe so the ML pipelines work-------------------------
		JavaPairRDD<Double, String> finalData = positiveData.union(negativeData);

		//Create DataFrame
		List<StructField> fields = new ArrayList<StructField>();
		fields.add(DataTypes.createStructField("text",  DataTypes.StringType, false));
		fields.add(DataTypes.createStructField("expected", DataTypes.DoubleType, false));
		StructType schema = DataTypes.createStructType(fields);

		@SuppressWarnings("unchecked")
		JavaRDD<Row> rows = finalData.map( new Function<Tuple2<Double, String>, Row>() {
			public Row call(Tuple2<Double, String> record) throws Exception {
				return RowFactory.create(record._2, record._1);
			}
		});

		DataFrame testData = sqlContext.createDataFrame(rows, schema);

		//Run test and save data
		//CHANGE THIS CALL DEPENDING ON WHETHER YOU WANT TO TEST A WHITELIST OR PIPELINE
		List<String> results = testPipeline(testData, args[1], sc, (int)positiveData.count());
		JavaRDD<String> toSave = sc.parallelize(results).repartition(1);
		toSave.saveAsTextFile(DARKMATTER_PREFIX + "models/" + "newer_whitelist_testResults.csv");
	}

	/*
	private static final int kFrequencyLimit = 8;
	public static List<String> testWhitelistClassifier(DataFrame testData, String models, JavaSparkContext sc, int numPositive) {

		ArrayList<String> testResults = new ArrayList<String>();
		testResults.add("test,precision,recall");
		JavaRDD<Row> rows = testData.toJavaRDD();

		//-----------------------------TEST THE MODEL-----------------------------
		for(String s : models.split(" ")){
			//Get the whitelist
			final List<String> whitelist = sc.textFile(DARKMATTER_PREFIX + "models/" + s).collect();


			for(int i = 1; i < kFrequencyLimit; i++) {
				System.out.println("Testing whitelist " + s + " with frequency " + i);
				final int temp = 1;

				final Accumulator<Integer> numIdentifiedAsRelevant = sc.accumulator(0);
				final Accumulator<Integer> numCorrectlyIdentifiedAsRelevant = sc.accumulator(0);

				rows.foreach(new VoidFunction<Row>() {
					public void call(Row row){

						int prediction = (new WhitelistFilter<Double>(temp, whitelist))
								.call(new Tuple2<Double, String>(row.getDouble(1), row.getString(0))) ? 1 : 0;
						int actual = (int) row.getDouble(1);

						numIdentifiedAsRelevant.add(prediction);
						numCorrectlyIdentifiedAsRelevant.add( (prediction == 1 && actual == 1) ? 1 : 0 );
					}
				});


				Double recall = ((double) numCorrectlyIdentifiedAsRelevant.value()) / numPositive;
				Double precision = ((double) numCorrectlyIdentifiedAsRelevant.value()) / 
						numIdentifiedAsRelevant.value();
				
				numIdentifiedAsRelevant.zero();
				numCorrectlyIdentifiedAsRelevant.zero();

				testResults.add(s + "/" + i + "," + precision.toString() + "," + recall.toString());
			}
		}
		return testResults;

	} */

	public static List<String> testPipeline(DataFrame testData, String models, JavaSparkContext sc, int numPositive) {

		ArrayList<String> testResults = new ArrayList<String>();
		testResults.add("test,precision,recall");

		//-----------------------------ACTUAL TESTING OF THE MODEL-----------------------------
		for(String s : models.split(" ")){
			PipelineModel model = PipelineModel.load(DARKMATTER_PREFIX + "models/" + s);
			DataFrame results  = model.transform(testData);
			JavaRDD<Row> resultsRows = results.javaRDD();
			final Accumulator<Integer> numIdentifiedAsRelevant = sc.accumulator(0);
			final Accumulator<Integer> numCorrectlyIdentifiedAsRelevant = sc.accumulator(0);

			//Nota Bene - foreach is a Spark *action*, so it is not evaluated lazily
			//This is the only way to make sure that the accumulators get ALL
			//the updates they need before computing the statistics
			//Spark *Transformations* like map or filter will not work here
			resultsRows.foreach(new VoidFunction<Row>() {
				public void call(Row row){
					int prediction = (int) row.getDouble(row.length() - 1);
					int actual = (int) row.getDouble(1);
					numIdentifiedAsRelevant.add(prediction);
					numCorrectlyIdentifiedAsRelevant.add( (prediction == 1 && actual == 1) ? 1 : 0 );
				}
			});

			Double recall = ((double) numCorrectlyIdentifiedAsRelevant.value()) / numPositive;
			Double precision = ((double) numCorrectlyIdentifiedAsRelevant.value()) / 
					numIdentifiedAsRelevant.value();

			testResults.add(s + "," + precision.toString() + "," + recall.toString());
		}
		return testResults;

	}
}
