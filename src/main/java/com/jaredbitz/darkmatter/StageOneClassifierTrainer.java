/*
 * Spark app that goes through the specified S3 bucket 
 * and trains a logistic classifier to recognize the topic of relevance. 
 * Negative examples are provided from a random segment of 
 * the CommonCrawl archives
 * 
 * 
 * USAGE: Takes four command line arguments:
 * spark-submit ... [topic] [model-location] [numPages] [fs]
 * 
 * topic: The subfolder within the s3 bucket containing the positive examples
 * model-location: what to name the saved model (in the "topic" subfolder)
 * numPages: how many pages used to train the model
 * fs: "s3n" if running locally, "s3" if running on EMR
 * 
 * It is intended for other 
 */

package com.jaredbitz.darkmatter;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.*;
import org.apache.spark.sql.*;

import com.jaredbitz.darkmatter.classifiers.*;

/**
 * Main class for the Spark app
 */
public class StageOneClassifierTrainer 
{
	public static void main( String[] args ) {
		
    	//Set up Spark Configuration, and initialize spark context
    	SparkConf conf = new SparkConf().setAppName("Dark Matter Classifier Trainer");
    	try {
    		conf.registerKryoClasses(new Class<?>[]{
    			Class.forName("org.apache.hadoop.io.LongWritable"),
    			Class.forName("org.apache.hadoop.io.Text")
    		});
    	} catch (ClassNotFoundException e) { //Should never happen, just java being safe
    		e.printStackTrace();
    	}
    	
    	JavaSparkContext sc = new JavaSparkContext(conf);
    	SQLContext sqlContext = new SQLContext(sc); //For DataFrames later
    	
    	System.out.println("created classifier");
    	ClassifierTrainer trainer = new CountVectorizerClassifierTrainer();
    	trainer.setConstants(args[0], args[1], args[2], args[3]);
    	trainer.trainClassifier(sc, sqlContext);
    }
}






        				