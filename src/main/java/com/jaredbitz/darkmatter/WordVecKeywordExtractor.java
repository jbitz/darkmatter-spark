/*
1 * Spark app that uses the Stanford NLP GloVe models of 
 * CommonCrawl to create a list of relevant keywords around
 * a given topic
 * 
 * Usage: spark-submit ... ["topic phrase"] [num]
 * 
 * Topic Phrase: The root keyword/phrase for the search
 * Num: Number of keywords to keep
 * 
 * Recommended usage is to pipe the output from stdout
 * into a text file to save
 * 
 * (This doesn't really need to be a spark app - I just did
 * it in the same format for consistency across this project)
 * It's perfectly fine to run locally without any dedicated
 * slave nodes, and each query into the full commoncrawl vector set
 * takes approximately 4-5 minutes on my i5 ultrabook
 */

package com.jaredbitz.darkmatter;

import java.util.ArrayList;
import java.util.List;
import org.apache.spark.*;
import org.apache.spark.api.java.*;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.mllib.linalg.*;
import org.apache.spark.sql.*;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import scala.Tuple2;

public class WordVecKeywordExtractor {
	
	//Set to discard all the terms that are below threshold in distance
	private static final double THRESHOLD = 0.0;
	
	public static void main(String[] args) {
		final String TARGET_PHRASE = args[0];
		final String VECTOR_URL = "vectors-full.txt.gz"; //Where to find the vector file (can be on s3 if needed)
		SparkConf conf = new SparkConf().setAppName("Dark Matter Word Vector Search");
		try {
			conf.registerKryoClasses(new Class<?>[]{
				Class.forName("org.apache.hadoop.io.LongWritable"),
				Class.forName("org.apache.hadoop.io.Text")
			});
		} catch (ClassNotFoundException e) { //Should never happen, just java being safe
			e.printStackTrace();
		}

		JavaSparkContext sc = new JavaSparkContext(conf);
		SQLContext sqlContext = new SQLContext(sc); //For DataFrames later
		
		//Can only access broadcast variables inline
		//Java 7 doesn't support lambdas
		//So this map function is REALLY messy, but it works
		JavaRDD<Tuple2<String, DenseVector>> wordVecs = sc.textFile(VECTOR_URL)
				.map( new Function<String, Tuple2<String, DenseVector>>() {
					public Tuple2<String, DenseVector> call(String line) {
						//First token is the word, rest are the vector
						String[] tokens = line.split(" ");
						double[] components = new double[tokens.length - 1];
						//Normalize the vector
						double sumSquared = 0;
						for(int i = 1; i < tokens.length; i++) {
							components[i - 1] = Double.parseDouble(tokens[i]);
							sumSquared += components[i - 1] * components[i - 1];
						}
						double normalizer = 1 / Math.sqrt(sumSquared);
						for(int i = 0; i < components.length; i++) {
							components[i] *= normalizer;
						}
						
						return new Tuple2<String, DenseVector>(tokens[0], 
								new DenseVector(components));
					}
				});
		
		//Collect all vectors related to the search phrase
		final List<Tuple2<String, DenseVector>> targetVectors = 
			wordVecs.filter( new Function<Tuple2<String, DenseVector>, Boolean>() {
				public Boolean call(Tuple2<String, DenseVector> record) throws Exception {
					for(String s : TARGET_PHRASE.split(" ")){
						if(s.equals(record._1)) return true;
					}
					return false;
				}
			}).collect();
		
		final DenseVector targetVector = generateTargetVector(targetVectors);
		
		//-------------------------Create data frame for queries-----------
		List<StructField> fields = new ArrayList<StructField>();
		fields.add(DataTypes.createStructField("word",  DataTypes.StringType, false));
		fields.add(DataTypes.createStructField("distance", DataTypes.DoubleType, false));
		StructType schema = DataTypes.createStructType(fields);

		@SuppressWarnings("unchecked")
		JavaRDD<Row> rows = wordVecs.map( new Function<Tuple2<String, DenseVector>, Row>() {
			public Row call(Tuple2<String, DenseVector> record) throws Exception {
				return RowFactory.create(record._1,
						calculateDistance(targetVector, record._2));
			}
		});
		
        DataFrame vectors = sqlContext.createDataFrame(rows, schema);
        vectors = vectors.where(vectors.col("distance").gt(THRESHOLD)).sort(vectors.col("distance").desc());
        for(Row r : vectors.takeAsList(Integer.parseInt(args[1]))){
        	System.out.println(r.get(0) + ": " +r.get(1));
        }
	}
	
	static DenseVector generateTargetVector(List<Tuple2<String, DenseVector>> targetVectors){
			
			//Sum the vectors
			double finalVecComponents[] = new double[targetVectors.get(0)._2.size()];
			for(Tuple2<String, DenseVector> tuple : targetVectors) {
				double[] components = tuple._2.toArray();
				for(int i = 0; i < finalVecComponents.length; i++) {
					finalVecComponents[i] += components[i];
				}
			}
			
			
			
			//Normalize them
			double sumSquared = 0;
			for(int i = 1; i < finalVecComponents.length; i++) {
				sumSquared += finalVecComponents[i - 1] * finalVecComponents[i - 1];
			}
			double normalizer = 1 / Math.sqrt(sumSquared);
			for(int i = 1; i < finalVecComponents.length; i++) {
				finalVecComponents[i] *= normalizer;
			}
			
			return new DenseVector(finalVecComponents);
		
	}

	static double calculateDistance(DenseVector v1, DenseVector v2){
		return org.apache.spark.mllib.linalg.BLAS.dot(v1, v2);
	}
}

class DenseVectorSaver implements AccumulatorParam<DenseVector>{
	DenseVector v;
	public DenseVector addInPlace(DenseVector arg0, DenseVector arg1) {
		v = arg1;
		return v;
	}

	public DenseVector zero(DenseVector arg0) {
		v = new DenseVector(new double[1]);
		return v;
	}

	public DenseVector addAccumulator(DenseVector arg0, DenseVector arg1) {
		v = arg1;
		return v;
	}
	
}