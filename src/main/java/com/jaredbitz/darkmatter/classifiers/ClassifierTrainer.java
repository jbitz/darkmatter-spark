/*
 * An abstract class from which to inherit when writing new classifiers.
 * Should be run from StageOneClassifierTrainer.java
 * See CountVectorizerClassifierTrainer for a concrete example.
 */

package com.jaredbitz.darkmatter.classifiers;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.ml.Pipeline;
import org.apache.spark.ml.PipelineModel;
import org.apache.spark.sql.*;
import org.apache.spark.sql.types.*;

import com.jaredbitz.darkmatter.functions.PositiveTransformer;
import com.jaredbitz.darkmatter.functions.WETFilter;
import com.jaredbitz.darkmatter.functions.WETTransformer;

import scala.Tuple2;

public abstract class ClassifierTrainer implements Serializable {
	
	protected final String DARKMATTER_BUCKET = "darkmatter-training"; //Project Bucket in S3
	protected String DARKMATTER_TOPIC; //Sub-folder containing training examples 
	protected String DARKMATTER_PREFIX;
	protected String S3_PROTOCOL;
	protected String OUTPUT_FILE;
	protected String WET_FILE_LIST;
	protected int NUM_TRAINING_EXAMPLES;
		
	/*
	 * Gets a JavaPairRDD of the positive training data
	 * The double is the label, and the string is the page text
	 */
	protected JavaPairRDD<Double, String> getPositiveData(JavaSparkContext sc) {
		List<Tuple2<String, String>> positiveExamples = sc.wholeTextFiles(DARKMATTER_PREFIX + "training-set/")
        		.take(NUM_TRAINING_EXAMPLES / 2);
        		
       return JavaPairRDD.fromJavaRDD(
        		sc.parallelizePairs(positiveExamples).map(new PositiveTransformer()));
	}
	
	/*
	 * Gets a JavaPairRDD of the negative training data
	 * The double is the label, and the string is the page text
	 * The default implementation just gets a random sample from CommonCrawl
	 */
	protected JavaPairRDD<Double, String> getNegativeData(JavaSparkContext sc) {
		//We only need the one random WET file for enough examples
		List<String> wetList = sc.textFile(WET_FILE_LIST).take(
        		new Random(System.currentTimeMillis()).nextInt(700));
        String ccURL = wetList.get(wetList.size() - 1);
        
        //Split WET file by page, rather than by line
        Configuration s3Conf = new Configuration();
        s3Conf.set("textinputformat.record.delimiter", "WARC/1.0");
        
        //Obtain WET file for negative (essentially random) examples and put it in the right format
        JavaPairRDD<LongWritable, Text> negativeExamples = sc.newAPIHadoopFile(S3_PROTOCOL 
        		+ "commoncrawl/" + ccURL, org.apache.hadoop.mapreduce.lib.input.TextInputFormat.class, 
        		LongWritable.class, Text.class, s3Conf);
        
        //Filter it down, keep only the page text, then return the correct # of pages
        negativeExamples = negativeExamples.filter(new WETFilter());
        return sc.parallelizePairs(negativeExamples.map(
        		new WETTransformer()).take(NUM_TRAINING_EXAMPLES / 2));
	}

	/*
	 * Builds the pipeline to process strings containing the entire text
	 * of a page and render them into bigram features for a logistic classifier
	 */
	protected abstract Pipeline buildPipeline(JavaSparkContext sc);
	
	//Where the magic happens
	public void trainClassifier(JavaSparkContext sc, SQLContext sqlContext) {
    	//Collect all the training data
		System.out.println("loading data");
    	JavaPairRDD<Double, String> positiveData = this.getPositiveData(sc);
        JavaPairRDD<Double, String> negativeData = this.getNegativeData(sc);
        JavaPairRDD<Double, String> trainingSet = negativeData.union(positiveData);
        
        //Set up the DataFrame (it's essentially a SQL table)
        StructType schema = this.createSchema();
		JavaRDD<Row> rows = trainingSet.map( new Function<Tuple2<Double, String>, Row>() {
			public Row call(Tuple2<Double, String> record) throws Exception {
					return RowFactory.create(record._1, record._2);
        	}
        });
        
        DataFrame trainingData = sqlContext.createDataFrame(rows, schema);
        
        //Begin training classifier pipeline
        
        System.out.println("building pipeline");
        Pipeline pipeline = this.buildPipeline(sc);
        
        //Fit the model and save it to S3
        PipelineModel model = pipeline.fit(trainingData);
        System.out.println("saving model");
        saveModel(model);
	}
	
	public void setConstants(String darkmatterTopic, String modelLocation,
			String numExamples, String s3Protocol){
		DARKMATTER_TOPIC = darkmatterTopic;
		S3_PROTOCOL = s3Protocol + "://";
		DARKMATTER_PREFIX = S3_PROTOCOL + DARKMATTER_BUCKET + '/' + DARKMATTER_TOPIC + '/';
		OUTPUT_FILE = DARKMATTER_PREFIX + "/models/" + modelLocation;
		WET_FILE_LIST = S3_PROTOCOL + 
				"commoncrawl/crawl-data/CC-MAIN-2016-22/wet.paths.gz"; //Master path file (from commoncrawl website)
		NUM_TRAINING_EXAMPLES = Integer.parseInt(numExamples);
	}
	
	protected StructType createSchema(){
        //There's no other word for this than clunky
		List<StructField> fields = new ArrayList<StructField>();
        fields.add(DataTypes.createStructField("label",  DataTypes.DoubleType, false));
        fields.add(DataTypes.createStructField("text",  DataTypes.StringType, false));
        return DataTypes.createStructType(fields);
	}
	
	/* Saves a given model to wherever it needs to be save
	 * (e.g. filesystem or S3)
	 */
	protected void saveModel(PipelineModel model) {
		try {
			model.save(this.OUTPUT_FILE);
	        System.out.println("Successfully saved file to S3!");

		} catch (IOException e) {
			e.printStackTrace();
		}       
	}
}
