/*
 * Simple classifier using bigram frequencies
 * as the feature set for a logistic regression
 * 
 * Positive examples are taken from a keyword-filtered Scrapy crawl 
 * stored on S3. Negative examples are from a random segment of 
 * CommonCrawl. Both are the defaults in ClassifierTrainer.java
 */

package com.jaredbitz.darkmatter.classifiers;

import java.util.List;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.ml.*;
import org.apache.spark.ml.PipelineStage;
import org.apache.spark.ml.classification.LogisticRegression;
import org.apache.spark.ml.feature.*;

import com.jaredbitz.darkmatter.functions.*;

import scala.Tuple2;

public class CountVectorizerClassifierTrainer extends ClassifierTrainer{
		
	/*
	 * Builds the pipeline to process strings containing the entire text
	 * of a page and render them into bigram features for a logistic classifier
	 */
	protected Pipeline buildPipeline(JavaSparkContext sc){
		Tokenizer tk = new Tokenizer().setInputCol("text").setOutputCol("tokens");
        
        StopWordsRemover stopwords = new StopWordsRemover().setInputCol("tokens")
        		.setOutputCol("filtered");
        
        stopwords.setStopWords(stopwords.getStopWords()); //Stopwords provided by spark
        
        NGram ngrams = new NGram().setInputCol("filtered").setOutputCol("ngrams")
        		.setN(2);
        
        CountVectorizer cv = new CountVectorizer().setInputCol("ngrams")
        		.setOutputCol("features").setMinTF(2);
        
        LogisticRegression lg = new LogisticRegression(); //Set params as needed
        
        
        Pipeline p = new Pipeline().setStages(new PipelineStage[] {
        		tk, stopwords, ngrams, cv, lg});
        
        return p;
	}

}
