package com.jaredbitz.darkmatter.functions;

import org.apache.spark.api.java.function.Function;
import scala.Tuple2;

/*
 * PositiveTransformer just assigns labels of 1.0 (positive example) to all
 * data from the Scrapy crawl
 */
public class PositiveTransformer implements Function<Tuple2<String, String>, Tuple2<Double, String>>{
	public Tuple2<Double, String> call(Tuple2<String, String> x){
		return new Tuple2<Double, String>(1.0, x._2.toString());
  	}
}
