package com.jaredbitz.darkmatter.functions;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.spark.api.java.function.Function;

import scala.Tuple2;

/*
 * WETFilter takes in tuples with WET files and filters them down to only
 * articles in English
 */
public class WETFilter implements Function<Tuple2<LongWritable, Text>, Boolean> {

	public Boolean call(Tuple2<LongWritable, Text> x){
  	return x._2.find(" the ") > -1 && //Dirty check for English
  			x._2.find("text/plain") > 1; //Make sure it's not metadata
  	}
}

