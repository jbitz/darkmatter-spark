package com.jaredbitz.darkmatter.functions;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.spark.api.java.function.Function;

import scala.Tuple2;

/*
 * WETTransformer assigns a label of 0.0 (negative example) to all
 * data from CommonCrawl, as well as changing around the types
 * from Hadoop Text to Java Strings
 */
public class WETTransformer implements Function<Tuple2<LongWritable, Text>, Tuple2<Double, String>> {
	@SuppressWarnings("unchecked")
	public Tuple2<Double, String> call(Tuple2<LongWritable, Text> x){
		return new Tuple2<Double, String>(0.0, x._2.toString().substring(x._2.find("Content-Length:") + 15));
  	}
}