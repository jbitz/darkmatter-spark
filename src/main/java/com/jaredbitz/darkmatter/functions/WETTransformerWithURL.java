package com.jaredbitz.darkmatter.functions;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.spark.api.java.function.Function;

import scala.Tuple2;

/*
 * WETTransformerWithURL takes a tuple of a LongWritable and Text from newAPIHadoopFile and changes it
 * to a tuple of two strings - the first is the URL, and the second is the content
 */
public class WETTransformerWithURL implements Function<Tuple2<LongWritable, Text>, Tuple2<String, String>> {
	@SuppressWarnings("unchecked")
	public Tuple2<String, String> call(Tuple2<LongWritable, Text> x){
		int URL_Start = x._2.find("WARC-Target-URI") + 17;
		int URL_End = x._2.find("WARC-Date") - 1; //Subtract one to get rid of the \n at the end of previous line

		//System.out.println(x._2);
		
		return new Tuple2<String, String>(
				x._2.toString().substring(URL_Start, URL_End), //URL
				x._2.toString().substring(x._2.find("Content-Length:") + 15)); //Content
  	}
}