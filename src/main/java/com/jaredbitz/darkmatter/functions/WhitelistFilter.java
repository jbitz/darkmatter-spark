/* WhitelistFilter.java
 * 
 * This represents the filtering algorithm to decide
 * whether or not a given page is on topic
 */

package com.jaredbitz.darkmatter.functions;

import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;

import org.apache.commons.collections.Bag;
import org.apache.commons.collections.bag.HashBag;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.Accumulator;


import scala.Tuple2;

/*
 * WETFilter takes in tuples with WET files and filters them down to only
 * articles in English
 */
public class WhitelistFilter<T> implements Function<Tuple2<T, String>, Boolean> {

	private double threshold = 0;
	private HashMap<String, Double> terms;
	private HashMap<String, Accumulator<Integer>> accs;

	public void setWhitelist(HashMap<String, Double> list) {
		this.terms = list;
	}

	public void setScore(double n) {
		this.threshold = n;
	}

	public WhitelistFilter(double score, HashMap<String, Double> list) {
		this.accs = null;
		this.setWhitelist(list);
		this.setScore(score);
	}

	public WhitelistFilter(double score, HashMap<String, Double> list,
			HashMap<String, Accumulator<Integer>> accumulators) {

		this.accs = accumulators;
		this.setWhitelist(list);
		this.setScore(score);
	}

	/* 
	 * The filtering function assesses the score for a document by 
	 * adding together the frequency for each key term (normalized 
	 * by document length), multiplied by that term's weight (the log
	 * of the average rating from three independent human raters)
	 * 
	 * Ultimately, here's the function:
	 * 
	 * for each keyword s:
	 *      documentScore += (TF(s) / Document_Length) * Weight(s)
	 * if documentScore > THRESHOLD: accept
	 * else:						 reject
	 * 
	 * where weight(s) = log( (score1(s) + score2(s) + score3(s)) / 3 )
	 */
	public Boolean call(Tuple2<T, String> x) {
		ArrayList<String> matches = new ArrayList<String>();
		double totalScore = 0;
		HashBag document = new HashBag(Arrays.asList(x._2.split(" ")));
		
		for(String s : terms.keySet()) {
			double weightedTF = ((double)document.getCount(s)) / ((double)document.size());
			if (weightedTF > 0)
				matches.add(s);
			totalScore += weightedTF * terms.get(s) * 1000;
		}
		if(totalScore >= threshold) {
			if(accs != null) {
				for(String m : matches)
					accs.get(m).add(1);
			}
			return true;
		}
		return false;
	}
}

